# Grupo de estudos de Django da Comunidade DevOpsPBS, projeto executado durando o estudo do livro "Django RESTful Web Services

Durante o estudo foi craido dois "apps", na construção de "toyss" foram aplicados os conhecimentos básicos dos cápitulos 1,2 e 3, e na contrução de "drones" foram aplicados todo o conhecimento adquirido pelo livro com exceção do cápitulo de teste, por estar realizando um estudo em outro livro, sendo assim, essa parte foi deixada para um segundo momento.

## Criar um ambiente de desenvolvimento Django com Docker

Com o git instalado e devidamente configurado rodar:

```console
git clone https://gitlab.com/devopspbs/django-docker-compose.git
cd django-docker-compose
```
Com docker e docker-compose devidamente instalados e configurados, crie o volume para o container do banco de dados, no caso vamos usar o nome "pgdate", se estiver em uso mude o nome do volume no docker-composer.yml:

```console
docker volume create pgdata
```

Se você estiver rodando Docker diretamente sobre Linux pode ser necessário ajustar as permissões. Lembre-se de repetir o comando abaixo sempre que estiver problemas para acessar os arquivos diretamente no Docker host.

```console
sudo chown -R $USER:$USER .
```

Por fim basta rodar `docker-compose up` para o API ficar acessível em http://localhost:8000/v1 a versão 1 e http://localhost:8000/v2 versão 2.

Para migrar os dados de teste utilizados no livro realize o seguinte procedimento:

```console
docker-compose exec web python manage.py loaddata data_dump.json
```

Executar o script de teste das "throttle roles":

```console
docker-compose exec web bash throttle_teste.sh
```

## Referências

https://gitlab.com/devopspbs/django/django-docker-compose

https://git-scm.com/downloads

https://docs.docker.com/install/

https://docs.docker.com/compose/install/

https://docs.docker.com/compose/django/

Gaston C. Hillar. Django RESTful Web Services. Packt Publishing Limited, January 2018. ISBN 9781788833929;

## License

GPLv3 or later.
